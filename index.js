let grey = false;

function setGrey() {
  const formBackground = document.querySelector("#loginForm");
  const navColor = document.querySelector("#myNav");
  const bodyBackGround = document.querySelector("body");
  const fbButton = document.querySelector(".fb-google-text");
  const redButton = document.querySelector("#redButton");
  const submitButton = document.querySelector("#account-btn");

  formBackground.style.backgroundColor = "rgb(207, 215, 220)";
  navColor.style.backgroundColor = "rgb(137, 137, 137, 0.4)";
  bodyBackGround.style.background =
    "linear-gradient(rgb(0, 0, 0, 0.65), rgb(0, 0, 0, 0.77)), url(./background-img/hw_7.png)";
  fbButton.style.backgroundColor = "rgb(207, 215, 220)";
  fbButton.style.border = "1px solid #3CB878";
  fbButton.style.color = "#000";
  redButton.style.backgroundColor = "rgb(207, 215, 220)";
  redButton.style.border = "1px solid #3CB878";
  redButton.style.color = "#000";
  submitButton.style.backgroundColor = "rgb(207, 215, 220)";
  submitButton.style.border = "1px solid #3CB878";
  submitButton.style.color = "#000";
  grey = true;
  localStorage.setItem('theme', 'grey');
}

function setDefault() {
  const formBackground = document.querySelector("#loginForm");
  const navColor = document.querySelector("#myNav");
  const bodyBackGround = document.querySelector("body");
  const fbButton = document.querySelector(".fb-google-text");
  const redButton = document.querySelector("#redButton");
  const submitButton = document.querySelector("#account-btn");

  formBackground.style.backgroundColor = "";
  navColor.style.backgroundColor = "";
  bodyBackGround.style.background = "";
  fbButton.style.backgroundColor = "";
  fbButton.style.border = "";
  fbButton.style.color = "";
  redButton.style.backgroundColor = "";
  redButton.style.border = "";
  redButton.style.color = "";
  submitButton.style.backgroundColor = "";
  submitButton.style.border = "";
  submitButton.style.color = "";
  grey = false;
  localStorage.removeItem('theme');
}

function toggleTheme() {
  if (!grey) {
    setGrey();
  } else {
    setDefault();
  }
}

function themeChange() {
  const lamp = document.querySelector("#lamp");
  lamp.addEventListener("click", toggleTheme);
}

window.addEventListener("DOMContentLoaded", () => {
  const savedTheme = localStorage.getItem("theme");
  if (savedTheme === "grey") {
    setGrey();
  }
  themeChange();
});
